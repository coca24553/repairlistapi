package com.ksa.repairlistapi.repository;

import com.ksa.repairlistapi.entity.RepairList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairListRepository extends JpaRepository<RepairList, Long> {
}
