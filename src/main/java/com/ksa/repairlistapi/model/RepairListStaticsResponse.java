package com.ksa.repairlistapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairListStaticsResponse {
    private Double allTotalPayment;
    private Double allRefundPrice;
    private Double ownExpense;
}
