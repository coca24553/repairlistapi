package com.ksa.repairlistapi.model;

import com.ksa.repairlistapi.enums.ReqairListEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RepairListRequest {
    private LocalDate accidentDate;
    private String accidentPlace;
    private ReqairListEnum reqairListEnum;
    private Boolean negligence;
    private String repairShop;
    private String maintenanceDetails;
    private Double totalPayment;
    private Boolean insuranceApplication;
    private Double refundPrice;
    private String attackerName;
    private String phoneNumber;
    private String etcMemo;
}
