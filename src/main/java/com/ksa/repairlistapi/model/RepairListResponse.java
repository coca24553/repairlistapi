package com.ksa.repairlistapi.model;

import com.ksa.repairlistapi.enums.ReqairListEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RepairListResponse {
    private Long id;
    private LocalDate accidentDate;
    private String accidentPlace;
    private String repairListEnum;
    private String negligence;
    private String repairShop;
    private String maintenanceDetails;
    private Double totalPayment;
    private String insuranceApplication;
    private Double refundPrice;
    private String attackerName;
    private String phoneNumber;
    private String etcMemo;
}
