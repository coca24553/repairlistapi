package com.ksa.repairlistapi.entity;

import com.ksa.repairlistapi.enums.ReqairListEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class RepairList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDate accidentDate;

    @Column
    private String accidentPlace;

    @Column
    @Enumerated
    private ReqairListEnum reqairListEnum;

    @Column
    private Boolean negligence;

    @Column
    private String repairShop;

    @Column
    private String maintenanceDetails;

    @Column
    private Double totalPayment;

    @Column
    private Boolean insuranceApplication;

    @Column
    private Double refundPrice;

    @Column
    private String attackerName;

    @Column
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
