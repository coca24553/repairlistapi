package com.ksa.repairlistapi.service;

import com.ksa.repairlistapi.entity.RepairList;
import com.ksa.repairlistapi.model.RepairListItem;
import com.ksa.repairlistapi.model.RepairListRequest;
import com.ksa.repairlistapi.model.RepairListResponse;
import com.ksa.repairlistapi.model.RepairListStaticsResponse;
import com.ksa.repairlistapi.repository.RepairListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RepairListService {
    private final RepairListRepository repairListRepository;

    public void setRepairList(RepairListRequest request) {
        RepairList addData = new RepairList();
        addData.setAccidentDate(LocalDate.now());
        addData.setAccidentPlace(request.getAccidentPlace());
        addData.setReqairListEnum(request.getReqairListEnum());
        addData.setNegligence(request.getNegligence());
        addData.setRepairShop(request.getRepairShop());
        addData.setMaintenanceDetails(request.getMaintenanceDetails());
        addData.setTotalPayment(request.getTotalPayment());
        addData.setInsuranceApplication(request.getInsuranceApplication());
        addData.setRefundPrice(request.getRefundPrice());
        addData.setAttackerName(request.getAttackerName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        repairListRepository.save(addData);
    }
    public List<RepairListItem> getRepairLists() {
        List<RepairList> originList = repairListRepository.findAll();

        List<RepairListItem> result = new LinkedList<>();

        for (RepairList repairList : originList) {
            RepairListItem addItem = new RepairListItem();
            addItem.setId(repairList.getId());
            addItem.setAccidentDate(repairList.getAccidentDate());
            addItem.setAccidentPlace(repairList.getAccidentPlace());
            addItem.setReqairListEnum(repairList.getReqairListEnum());
            addItem.setNegligence(repairList.getNegligence());
            addItem.setRepairShop(repairList.getRepairShop());
            addItem.setMaintenanceDetails(repairList.getMaintenanceDetails());
            addItem.setTotalPayment(repairList.getTotalPayment());
            addItem.setInsuranceApplication(repairList.getInsuranceApplication());
            addItem.setRefundPrice(repairList.getRefundPrice());
            addItem.setAttackerName(repairList.getAttackerName());
            addItem.setPhoneNumber(repairList.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public RepairListResponse getRepairList(long id) {
        RepairList originData = repairListRepository.findById(id).orElseThrow();

        RepairListResponse response = new RepairListResponse();
        response.setId(originData.getId());
        response.setAccidentDate(originData.getAccidentDate());
        response.setAccidentPlace(originData.getAccidentPlace());
        response.setRepairListEnum(originData.getReqairListEnum().getName());
        response.setNegligence(originData.getNegligence() ? "쌍방 과실" : "일반 과실");
        response.setRepairShop(originData.getRepairShop());
        response.setMaintenanceDetails(originData.getMaintenanceDetails());
        response.setTotalPayment(originData.getTotalPayment());
        response.setInsuranceApplication(originData.getInsuranceApplication() ? "Yes" : "No");
        response.setRefundPrice(originData.getRefundPrice());
        response.setAttackerName(originData.getAttackerName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public RepairListStaticsResponse getStatics() {
        RepairListStaticsResponse response = new RepairListStaticsResponse();

        List<RepairList> originList = repairListRepository.findAll();

        double allTotalPayment = 0D;
        double allRefundPrice = 0D;
        for (RepairList repairList : originList) {
            allTotalPayment += repairList.getTotalPayment();
            allRefundPrice += repairList.getRefundPrice();
        }

        double ownExpense = allTotalPayment - allRefundPrice;

        response.setAllTotalPayment(allTotalPayment);
        response.setAllRefundPrice(allRefundPrice);
        response.setOwnExpense(ownExpense);

        return response;
    }
}
