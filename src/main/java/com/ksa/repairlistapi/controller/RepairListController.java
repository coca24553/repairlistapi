package com.ksa.repairlistapi.controller;

import com.ksa.repairlistapi.model.RepairListItem;
import com.ksa.repairlistapi.model.RepairListRequest;
import com.ksa.repairlistapi.model.RepairListResponse;
import com.ksa.repairlistapi.model.RepairListStaticsResponse;
import com.ksa.repairlistapi.service.RepairListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/repair-list")
public class RepairListController {
    private final RepairListService repairListService;

    @PostMapping("/new")
    public String setRepairList(@RequestBody RepairListRequest request) {
        repairListService.setRepairList(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<RepairListItem> getRepairLists() {
        return repairListService.getRepairLists();
    }

    @GetMapping("/detail/{id}")
    public RepairListResponse getRepairList(@PathVariable long id) {
        return repairListService.getRepairList(id);
    }

    @GetMapping("/statics")
    public RepairListStaticsResponse getStatics() {
        return repairListService.getStatics();
    }
}
