package com.ksa.repairlistapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ReqairListEnum {
    CAR_CAR("차&차 사고"),
    CAR_HUMAN("차&사람 사고"),
    SINGLE("단독 사고");

    private final String name;
}
